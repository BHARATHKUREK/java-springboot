package com.lowes.university.service;

import com.lowes.university.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UniversityService {

    public University getUniversityDetails(){

        University university = new University();

        Location universityLocation = new Location();
        universityLocation.setCountry("India");
        universityLocation.setState("Karnataka");
        universityLocation.setDistrict("Belgum");
        universityLocation.setPinCode(77777);

        university.setId(1);
        university.setName("VTU");
        university.setLocation(universityLocation);

        List<College> collegeList = new ArrayList<>();

        //College 1 details
        College college1 = new College();
        college1.setName("SIT");

        Location location1 = new Location();
        location1.setCountry("India");
        location1.setState("karnataka");
        location1.setDistrict("Tumkur");
        location1.setPinCode(562345);

        college1.setLocation(location1);

        collegeList.add(college1);


        //College 2 details

        College college2 = new College();
        college2.setName("BMS");

        Location location2 = new Location();
        location2.setCountry("India");
        location2.setState("karnataka");
        location2.setDistrict("Banglore");
        location2.setPinCode(567890);

        college2.setLocation(location2);
        collegeList.add(college2);

        //return data to university
        university.setAffiliatedColleges(collegeList);


        university.setSupportedCourse(getCourse());
        return university;

    }

    public List<Course> getCourse(){
        List<Course> courseList = new ArrayList<>();
        Course cseCourse1 = new Course();
        cseCourse1.setCourseId(1);
        cseCourse1.setCourseName("CSE");
        Map<Integer,List<Subject>> firstYearCSESubject= new HashMap<>();
        firstYearCSESubject.put(1,cseGetSubject());
        cseCourse1.setSubjectsWRTYear(firstYearCSESubject);
        courseList.add(cseCourse1);

        Course eceCourse1 = new Course();
        eceCourse1.setCourseId(1);
        eceCourse1.setCourseName("ECE");
        Map<Integer,List<Subject>> firstYearECESubject = new HashMap<>();
        firstYearECESubject.put(1,eceGetSubject());
        eceCourse1.setSubjectsWRTYear(firstYearECESubject);
        courseList.add(eceCourse1);

        //second year subject
       /* Course secondCourse = new Course();
        secondCourse.setCourseId(1);
        secondCourse.setCourseName("ECE");
        Map<Integer,List<Subject>> secondYearSubject= new HashMap<>();
        secondYearSubject.put(2,getSubject());
        secondCourse.setSubjectsWRTYear(secondYearSubject);
        courseList.add(secondCourse);*/

        return courseList;
    }

    public List<Subject> cseGetSubject(){
        List<Subject> subjectList = new ArrayList<>();

        Subject subject1 = new Subject();
        subject1.setSubjectId(1);
        subject1.setSubjectName("Operating system");
        subject1.setSubjectToughtBy(getLecturer());
        subjectList.add(subject1);

        Subject subject2 = new Subject();
        subject2.setSubjectId(1);
        subject2.setSubjectName("Microservice");
        subject2.setSubjectToughtBy(getLecturer());
        subjectList.add(subject2);

        Subject subject3 = new Subject();
        subject3.setSubjectId(1);
        subject3.setSubjectName("Signals and Systems");
        subject3.setSubjectToughtBy(getLecturer());
        subjectList.add(subject3);

        return subjectList;
    }

    public List<Subject> eceGetSubject(){
        List<Subject> subjectList = new ArrayList<>();

        Subject subject1 = new Subject();
        subject1.setSubjectId(1);
        subject1.setSubjectName("Digital electronics");
        subject1.setSubjectToughtBy(getLecturer());
        subjectList.add(subject1);

        Subject subject2 = new Subject();
        subject2.setSubjectId(1);
        subject2.setSubjectName("Analog electronics");
        subject2.setSubjectToughtBy(getLecturer());
        subjectList.add(subject2);

        Subject subject3 = new Subject();
        subject3.setSubjectId(1);
        subject3.setSubjectName("Power electronics");
        subject3.setSubjectToughtBy(getLecturer());
        subjectList.add(subject3);

        return subjectList;
    }

    public List<Lecturer> getLecturer(){
        List<Lecturer> lecturers = new ArrayList<>();
        Lecturer lecturer1 = new Lecturer();
        lecturer1.setAge(25);
        lecturer1.setDesignation("Professor");
        lecturer1.setGeneder("Male");
        lecturer1.setId(123);
        lecturer1.setName("RAM");
        lecturers.add(lecturer1);

        Lecturer lecturer2 = new Lecturer();
        lecturer2.setAge(26);
        lecturer2.setDesignation("Professor");
        lecturer2.setGeneder("Male");
        lecturer2.setId(120);
        lecturer2.setName("Josh");
        lecturers.add(lecturer2);

        return lecturers;
    }
}
