package com.lowes.university.domain;

import java.util.List;

public class University {

    private long id;
    private String name;
    private List<College> affiliatedColleges;
    private Location location;
    private List<Course> supportedCourse;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<College> getAffiliatedColleges() {
        return affiliatedColleges;
    }

    public void setAffiliatedColleges(List<College> affiliatedColleges) {
        this.affiliatedColleges = affiliatedColleges;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Course> getSupportedCourse() {
        return supportedCourse;
    }

    public void setSupportedCourse(List<Course> supportedCourse) {
        this.supportedCourse = supportedCourse;
    }

}
