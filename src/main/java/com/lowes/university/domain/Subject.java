package com.lowes.university.domain;

import java.util.List;

public class Subject {

    private String subjectName;
    private long subjectId;
    private List<Lecturer> subjectToughtBy;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public List<Lecturer> getSubjectToughtBy() {
        return subjectToughtBy;
    }

    public void setSubjectToughtBy(List<Lecturer> subjectToughtBy) {
        this.subjectToughtBy = subjectToughtBy;
    }
}
