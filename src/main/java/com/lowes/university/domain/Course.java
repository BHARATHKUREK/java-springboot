package com.lowes.university.domain;

import java.util.List;
import java.util.Map;

public class Course {

    private long courseId;
    private String courseName;
    private Map<Integer,List<Subject>> subjectsWRTYear;

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Map<Integer, List<Subject>> getSubjectsWRTYear() {
        return subjectsWRTYear;
    }

    public void setSubjectsWRTYear(Map<Integer, List<Subject>> subjectsWRTYear) {
        this.subjectsWRTYear = subjectsWRTYear;
    }
}
