package com.lowes.university.domain;

public class Lecturer {

    private String name;
    private int id;
    private String gender;
    private String designation;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGeneder() {
        return gender;
    }

    public void setGeneder(String geneder) {
        this.gender = geneder;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
