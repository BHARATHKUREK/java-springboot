package com.lowes.university.controller;

import com.lowes.university.domain.Course;
import com.lowes.university.domain.Subject;
import com.lowes.university.domain.University;
import com.lowes.university.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class UniversityController {

    @Autowired
    private UniversityService universityService;

    @GetMapping("/getUniversityDetails")
    public University getUniversityDetails(){
        University universityOutput  = universityService.getUniversityDetails();
        return  universityOutput;
    }
}
